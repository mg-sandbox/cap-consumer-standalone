sap.ui.define([
        "sap/ui/core/mvc/Controller",
        "sap/m/MessageToast"
	],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
	function (Controller, MessageToast) {
		"use strict";

		return Controller.extend("mg.cap.cons.sa.fiorimodule.controller.App", {
			onInit: function () {
                // Do something...
            },
            loadHelloWorld: function(){
                sap.ui.require(["provider/control/helloworld"], function(oControl) {
                    console.log("Load helloworld control");
                    console.log(new oControl());
                });
            },
            loadGoodbyeWorld: function(){
                sap.ui.require(["provider/control/goodbyeworld"], function(oControl) {
                    console.log("Load goodbyeworld control");
                    console.log(new oControl());
                });
            },
            handleButton: function(){
                MessageToast.show("Consumer application button is pressed");
            },
            handleAddButton: function(){
                var oContext = this.getView().byId("BookTable").getBinding("items").create({
                    "ID": 0, "title": "", "stock": 0
                });    
                oContext.created().then(function () {
                        MessageToast.show("Enter the name of the book...");
                    }, function (oError) {
                        MessageToast.show("Oops something went wrong...");
                    }
                );                
            },
            handleDeleteButton: function(oEvent){
                var oTable = this.getView().byId("BookTable");
                var oContext = oEvent.getParameters().listItem.getBindingContext();
                oContext.delete("$auto").then(function () {
                    oTable.removeSelections();
                    MessageToast.show("Book deleted");
                }, function (oError) {
                    MessageToast.show("Oops book not deleted...");
                });
            }
		});
	});
