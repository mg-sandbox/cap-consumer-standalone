const cds = require('@sap/cds')

module.exports = async (server) => {
    const db = await cds.connect.to("db");
    const {Books} = db.entities;

    server.before("CREATE", "Books", async req => {
        const {ID} = await SELECT.one.from(Books).columns('max(ID) as ID');
        req.data.ID = ID+1;
    });
};
