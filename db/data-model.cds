namespace mg.cap.consumer.bookshop;

entity Books {
  key ID : Integer;
  title  : String;
  stock  : Integer;
}